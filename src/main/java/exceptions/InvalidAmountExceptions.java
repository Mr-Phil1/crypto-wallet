package exceptions;

public class InvalidAmountExceptions extends Exception {
    public InvalidAmountExceptions() {
        super("Invalid Amount < 0!");
    }
}
