package ui;

import at.itkolleg.sample.WalletApp;
import domain.CryptoCurrency;
import domain.CurrentPriceForCurrency;
import domain.Wallet;
import exceptions.InsufficientBalanceException;
import exceptions.InvalidFeeException;
import infrastruktur.CurrentCurrencyPrices;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.math.BigDecimal;
import java.util.Optional;

public class MainController extends BaseControllerState {

    @FXML
    private Button btnClose;

    @FXML
    private ComboBox cmbWalletCurrency;

    @FXML
    private Label lblBankaccountBalance;

    @FXML
    private TableView<Wallet> tableView;

    public void initialize() {
        this.cmbWalletCurrency.getItems().addAll(CryptoCurrency.getCodes());
        this.lblBankaccountBalance.textProperty().setValue(getBankAccount().getBalance().toString() + "€");

        TableColumn<Wallet, String> symbol = new TableColumn<>("SYMBOL");
        symbol.setCellValueFactory(new PropertyValueFactory<>("CRYPTO_CURRENCY"));

        TableColumn<Wallet, String> currencyName = new TableColumn<>("CURRENCY NAME");
        currencyName.setCellValueFactory(new PropertyValueFactory<>("currencyName"));

        TableColumn<Wallet, String> name = new TableColumn<>("WALLET NAME");
        name.setCellValueFactory(new PropertyValueFactory<>("NAME"));

        TableColumn<Wallet, String> amount = new TableColumn<>("AMOUNT");
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));

        TableColumn<Wallet, String> currentCurrencyPrice = new TableColumn<>("CURRENT CURRENCY PRICE");
        currentCurrencyPrice.setCellValueFactory(new PropertyValueFactory<>("CURRENT_CURRENCY_PRICES"));

        tableView.getColumns().clear();
        tableView.getColumns().add(name);
        tableView.getColumns().add(symbol);
        tableView.getColumns().add(currencyName);
        tableView.getColumns().add(amount);
        tableView.getColumns().add(currentCurrencyPrice);

        name.setMinWidth(150);
        name.setSortable(false);
        symbol.setMinWidth(80);
        symbol.setSortable(true);
        currencyName.setMinWidth(140);
        currencyName.setSortable(false);
        amount.setMinWidth(80);
        amount.setSortable(false);
        currentCurrencyPrice.setMinWidth(200);
        currentCurrencyPrice.setSortable(false);

        //symbol.prefWidthProperty().bind(tableView.widthProperty().multiply(0.2));
        //amount.prefWidthProperty().bind(tableView.widthProperty().add(3));
        tableView.getItems().setAll(getWalletList().getWalletAsObservableList());
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        this.btnClose.setOnAction((ActionEvent e) -> {
            Platform.exit();

        });

    }

    public void deposit() {
        TextInputDialog dialog = new TextInputDialog("Insert amount to deposit ...");
        dialog.setTitle("Deposit to bankaccount");
        dialog.setHeaderText("How much money do you want to deposit?");
        dialog.setContentText("Amount: ");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            try {
                BigDecimal amount = new BigDecimal(result.get());
                this.getBankAccount().deposit(amount);
                this.lblBankaccountBalance.textProperty().setValue(this.getBankAccount().getBalance().toString());
            } catch (NumberFormatException numberFormatException) {
                WalletApp.showErrorDialog("Please input a number!");
            }
        }
    }

    public void withdraw() {
        TextInputDialog dialog = new TextInputDialog("Insert amount to withdraw ...");
        dialog.setTitle("Withdraw to bankaccount");
        dialog.setHeaderText("How much money do you want to withdraw?");
        dialog.setContentText("Amount: ");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            try {
                BigDecimal amount = new BigDecimal(result.get());
                this.getBankAccount().withdraw(amount);
                this.lblBankaccountBalance.textProperty().setValue(this.getBankAccount().getBalance().toString());
            } catch (NumberFormatException numberFormatException) {
                WalletApp.showErrorDialog("Please input a number!");
            } catch (InsufficientBalanceException insufficientBalanceException) {
                WalletApp.showErrorDialog(insufficientBalanceException.getMessage());
            }
        }
    }

    public void openWallet() {
        Wallet wallet = this.tableView.getSelectionModel().getSelectedItem();
        if (wallet != null) {
            GlobalContext.getGlobalContext().putStateFor(WalletApp.globlaSelectedWallet, wallet);
            WalletApp.switchScene("wallet.fxml", "at.itkolleg.sample.wallet");
        } else {
            WalletApp.showErrorDialog("You have select a wallet first!");
        }

    }

    public void newWallet() throws InvalidFeeException {
        Object selectedItem = this.cmbWalletCurrency.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            WalletApp.showErrorDialog("Choose currency!");
            return;
        }
        CryptoCurrency selectedCryptoCurrency = CryptoCurrency.valueOf(this.cmbWalletCurrency.getSelectionModel().getSelectedItem().toString());
        this.getWalletList().addWallet(new Wallet("My " + selectedCryptoCurrency.currencyName + " Wallet", selectedCryptoCurrency, new BigDecimal("1")));
        tableView.getItems().setAll(this.getWalletList().getWalletAsObservableList());
    }
}


