package ui;

import at.itkolleg.sample.WalletApp;
import domain.CurrentPriceForCurrency;
import domain.Transaction;
import domain.Wallet;
import exceptions.GetCurrentPriceException;
import exceptions.InsufficientAmountException;
import exceptions.InsufficientBalanceException;
import exceptions.InvalidAmountExceptions;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;


public class WalletController extends BaseControllerState {

    private String currencySymbol = "€";
    @FXML
    private Button btnBackToMain;

    @FXML
    private Label lblId, lblName, lblCurrency, lblAmount, lblFee, lblValue, lblCurrentCurrencyPrice;

    @FXML
    private TableView<Transaction> tblTransactions;

    private Wallet wallet;

    public void initialize() {
        this.wallet = (Wallet) GlobalContext.getGlobalContext().getStateFor(WalletApp.globlaSelectedWallet);
        populateTable();
        refreshAllGuiValue();
        btnBackToMain.setOnAction((ActionEvent e) -> {
            WalletApp.switchScene("main.fxml", "at.itkolleg.sample.main");
        });
    }

    private CurrentPriceForCurrency getCurrentPriceStrategy() {
        return (CurrentPriceForCurrency) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_CURRENT_PRICES);
    }

    private void refreshAllGuiValue() {
        this.lblName.textProperty().setValue(this.wallet.getNAME());
        this.lblId.textProperty().setValue(this.wallet.getID().toString());
        this.lblCurrency.textProperty().setValue(this.wallet.getCRYPTO_CURRENCY().getCode());
        this.lblAmount.textProperty().setValue(this.wallet.getAmount().toString());
        this.lblFee.textProperty().setValue(this.wallet.getFeeInPercent().toString());
        try {
            this.lblCurrentCurrencyPrice.textProperty().setValue(this.getCurrentPriceStrategy().getCurrentPrice(this.wallet.getCRYPTO_CURRENCY()).toString()+"€");
        } catch (GetCurrentPriceException currentPriceException) {
            WalletApp.showErrorDialog(currentPriceException.getMessage());
            lblValue.textProperty().setValue("CURRENT PRICES UNAVAILABLE!");

        }

        try {
            BigDecimal currentPrice = this.getCurrentPriceStrategy().getCurrentPrice(this.wallet.getCRYPTO_CURRENCY());
            BigDecimal currentValue = currentPrice.multiply(this.wallet.getAmount()).setScale(6, RoundingMode.HALF_UP);
            lblValue.textProperty().setValue(currentValue.toString() + currencySymbol);
        } catch (GetCurrentPriceException getCurrentPriceException) {
            WalletApp.showErrorDialog(getCurrentPriceException.getMessage());
            lblValue.textProperty().setValue("CURRENT PRICES UNAVAILABLE!");
        }
        tblTransactions.getItems().setAll(this.wallet.getTRANSACTIONS());
    }

    private void populateTable() {
        TableColumn<Transaction, String> id = new TableColumn<>("ID");
        id.setCellValueFactory(new PropertyValueFactory<>("ID"));

        TableColumn<Transaction, String> crypto = new TableColumn<>("CRYPTO");
        crypto.setCellValueFactory(new PropertyValueFactory<>("CRYPTO_CURRENCY"));

        TableColumn<Transaction, String> amount = new TableColumn<>("AMOUNT");
        amount.setCellValueFactory(new PropertyValueFactory<>("AMOUNT"));

        TableColumn<Transaction, String> total = new TableColumn<>("TOTAL");
        total.setCellValueFactory(new PropertyValueFactory<>("TOTAL"));

        TableColumn<Transaction, String> priceAtTransactionDate = new TableColumn<>("PRICE_AT_TRANSACTION_DATE");
        priceAtTransactionDate.setCellValueFactory(new PropertyValueFactory<>("PRICE_AT_TRANSACTION_DATE"));

        TableColumn<Transaction, String> date = new TableColumn<>("DATE");
        date.setCellValueFactory(new PropertyValueFactory<>("DATE"));

        tblTransactions.getColumns().clear();
        tblTransactions.getColumns().add(id);
        tblTransactions.getColumns().add(crypto);
        tblTransactions.getColumns().add(amount);
        tblTransactions.getColumns().add(total);
        tblTransactions.getColumns().add(priceAtTransactionDate);
        tblTransactions.getColumns().add(date);


        tblTransactions.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    }

    public void buy() {
        TextInputDialog dialog = new TextInputDialog("Amount of crypto to buy ...");
        dialog.setTitle("Buy Crypto");
        dialog.setHeaderText("How much crypto do you want to buy?");
        dialog.setContentText("Amount: ");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            try {
                BigDecimal amount = new BigDecimal(result.get());
                this.wallet.buy(amount, this.getCurrentPriceStrategy().getCurrentPrice(this.wallet.getCRYPTO_CURRENCY()), this.getBankAccount());
                this.refreshAllGuiValue();
            } catch (NumberFormatException numberFormatException) {
                WalletApp.showErrorDialog("Invalid amount. Insert a number!");
            } catch (GetCurrentPriceException | InsufficientBalanceException | InvalidAmountExceptions exception) {
                WalletApp.showErrorDialog(exception.getMessage());
                WalletApp.exceptionDialog(exception, exception.getMessage());
            }
        }
    }

    public void sell() {
        TextInputDialog dialog = new TextInputDialog("Amount of crypto to sell ...");
        dialog.setTitle("Sell Crypto");
        dialog.setHeaderText("How much crypto do you want to sell?");
        dialog.setContentText("Amount: ");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            try {
                BigDecimal amount = new BigDecimal(result.get());
                this.wallet.sell(amount, this.getCurrentPriceStrategy().getCurrentPrice(this.wallet.getCRYPTO_CURRENCY()), this.getBankAccount());
                this.refreshAllGuiValue();
            } catch (NumberFormatException numberFormatException) {
                WalletApp.showErrorDialog("Invalid amount. Insert a number!");
            } catch (GetCurrentPriceException | InvalidAmountExceptions | InsufficientAmountException exception) {
                WalletApp.showErrorDialog(exception.getMessage());
                WalletApp.exceptionDialog(exception, exception.getMessage());
            }
        }


    }
}
