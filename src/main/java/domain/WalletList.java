package domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class WalletList implements Serializable {
    private final HashMap<CryptoCurrency, Wallet> WALLETS;

    public WalletList() {
        this.WALLETS = new HashMap<>();
    }

    public void addWallet(Wallet wallet) {
        if (wallet != null && !this.WALLETS.containsKey(wallet.getCRYPTO_CURRENCY())) {
            this.WALLETS.put(wallet.getCRYPTO_CURRENCY(), wallet);
        }
    }

    public Wallet getWallet(CryptoCurrency cryptoCurrency) {
        return this.WALLETS.get(cryptoCurrency);
    }

    public List<Wallet> getWalletAsObservableList() {
        return WALLETS.values().stream().collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "WalletList{" +
                "WALLETS=" + WALLETS +
                '}';
    }
}
