package domain;

import at.itkolleg.sample.WalletApp;
import exceptions.*;
import infrastruktur.CurrentCurrencyPrices;
import ui.GlobalContext;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Wallet implements Serializable {
    private final UUID ID;
    private final String NAME;
    private final CryptoCurrency CRYPTO_CURRENCY;
    private BigDecimal amount;
    private BigDecimal feeInPercent;
    private final CurrentCurrencyPrices CURRENT_CURRENCY_PRICES;
    private final List<Transaction> TRANSACTIONS;

    public Wallet(String NAME, CryptoCurrency CRYPTO_CURRENCY, BigDecimal feeInPercent) throws InvalidFeeException {
        this.ID = UUID.randomUUID();
        this.NAME = NAME;
        this.CRYPTO_CURRENCY = CRYPTO_CURRENCY;
        this.amount = new BigDecimal("0");
        this.TRANSACTIONS = new ArrayList<>();
        this.setNewFee(feeInPercent);
        this.CURRENT_CURRENCY_PRICES = (CurrentCurrencyPrices) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_CURRENT_PRICES);
    }

    public void buy(BigDecimal amount, BigDecimal currentPrice, BankAccount bankAccount) throws InvalidAmountExceptions, InsufficientBalanceException {
        if (amount.compareTo(new BigDecimal("0")) <= 0) {
            throw new InvalidAmountExceptions();
        }
        Transaction transaction = new Transaction(this.CRYPTO_CURRENCY, amount, currentPrice.setScale(6, RoundingMode.HALF_UP));
        bankAccount.withdraw(transaction.getTOTAL().multiply(new BigDecimal("100").add(feeInPercent).divide(new BigDecimal("100"))).setScale(6, RoundingMode.HALF_UP));
        this.TRANSACTIONS.add(transaction);
        this.amount = this.amount.add(transaction.getAMOUNT());

    }

    public UUID getID() {
        return ID;
    }

    public String getNAME() {
        return NAME;
    }

    public CryptoCurrency getCRYPTO_CURRENCY() {
        return CRYPTO_CURRENCY;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getFeeInPercent() {
        return feeInPercent;
    }

    public List<Transaction> getTRANSACTIONS() {
        return List.copyOf(TRANSACTIONS);
    }

    public String getCurrencyName() {
        return this.CRYPTO_CURRENCY.getCurrencyName();
    }

    public void setNewFee(BigDecimal fee) throws InvalidFeeException {
        if (fee.compareTo(new BigDecimal("0")) >= 0) {
            this.feeInPercent = fee;
        } else {
            throw new InvalidFeeException();
        }
    }

    public void sell(BigDecimal amount, BigDecimal currentPrice, BankAccount bankAccount) throws InsufficientAmountException, InvalidAmountExceptions {
        if (amount.compareTo(new BigDecimal("0")) <= 0) {
            throw new InvalidAmountExceptions();
        }
        BigDecimal reducedAmount = this.amount.subtract(amount);
        if (reducedAmount.compareTo(new BigDecimal("0")) < 0) {
            throw new InsufficientAmountException();
        }
        Transaction transaction = new Transaction(this.CRYPTO_CURRENCY, amount.negate(), currentPrice.setScale(6, RoundingMode.HALF_UP));
        bankAccount.deposit(transaction.getTOTAL().negate().multiply(new BigDecimal("100").subtract(feeInPercent).divide(new BigDecimal("100"))).setScale(6, RoundingMode.HALF_UP));
        this.TRANSACTIONS.add(transaction);
        this.amount = reducedAmount;
    }

    public BigDecimal getCURRENT_CURRENCY_PRICES() {
        BigDecimal tmp;
        try {
            tmp = CURRENT_CURRENCY_PRICES.getCurrentPrice(this.getCRYPTO_CURRENCY());
        } catch (GetCurrentPriceException getCurrentPriceException) {
            WalletApp.showErrorDialog(getCurrentPriceException.getMessage());
            tmp = new BigDecimal("0");
            WalletApp.exceptionDialog(getCurrentPriceException, getCurrentPriceException.getMessage());
        }
        return tmp;
    }

    @Override
    public String toString() {
        return "Wallet{" +
                "ID=" + ID +
                ", NAME='" + NAME + '\'' +
                ", CRYPTO_CURRENCY=" + CRYPTO_CURRENCY +
                ", amount=" + amount +
                ", TRANSACTIONS=" + TRANSACTIONS +
                ", feeInPercent=" + feeInPercent +
                '}';
    }
}
