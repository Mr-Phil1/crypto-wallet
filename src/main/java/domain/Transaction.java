package domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.UUID;

public class Transaction implements Serializable {
    private final UUID ID;
    private final CryptoCurrency CRYPTO_CURRENCY;
    private final BigDecimal AMOUNT;
    private final BigDecimal PRICE_AT_TRANSACTION_DATE;
    private final BigDecimal TOTAL;
    private final LocalDate DATE;

    public Transaction(CryptoCurrency CRYPTO_CURRENCY, BigDecimal AMOUNT, BigDecimal PRICE_AT_TRANSACTION_DATE) {
        this.ID=UUID.randomUUID();
        this.CRYPTO_CURRENCY = CRYPTO_CURRENCY;
        this.AMOUNT = AMOUNT;
        this.PRICE_AT_TRANSACTION_DATE = PRICE_AT_TRANSACTION_DATE;
        this.DATE=LocalDate.now();
        this.TOTAL=this.AMOUNT.multiply(this.PRICE_AT_TRANSACTION_DATE).setScale(6, RoundingMode.HALF_UP);
    }

    public UUID getID() {
        return ID;
    }

    public CryptoCurrency getCRYPTO_CURRENCY() {
        return CRYPTO_CURRENCY;
    }

    public BigDecimal getAMOUNT() {
        return AMOUNT;
    }

    public BigDecimal getPRICE_AT_TRANSACTION_DATE() {
        return PRICE_AT_TRANSACTION_DATE;
    }

    public BigDecimal getTOTAL() {
        return TOTAL;
    }

    public LocalDate getDATE() {
        return DATE;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "ID=" + ID +
                ", CRYPTO_CURRENCY=" + CRYPTO_CURRENCY +
                ", AMOUNT=" + AMOUNT +
                ", PRICE_AT_TRANSATION_DATE=" + PRICE_AT_TRANSACTION_DATE +
                ", DATE=" + DATE +
                ", TOTAL=" + TOTAL +
                '}';
    }
}
