package infrastruktur;

import domain.CryptoCurrency;
import domain.CurrentPriceForCurrency;
import exceptions.GetCurrentPriceException;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class CurrentCurrencyPrices implements CurrentPriceForCurrency, Serializable {
    @Override
    public BigDecimal getCurrentPrice(CryptoCurrency cryptoCurrency) throws GetCurrentPriceException {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder(
                URI.create("https://api.coingecko.com/api/v3/simple/price?ids="
                        + cryptoCurrency.getCurrencyName()
                        + "&vs_currencies=eur")
        ).header("accept", "application/json").build();
        try {
            HttpResponse<String> result = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            String[] split = result.body().split(":");
            String result2 = split[2].substring(0, split[2].length() - 2);
            return new BigDecimal(result2);
        } catch (IOException iOException) {
            iOException.printStackTrace();
            throw new GetCurrentPriceException("IOException: " + iOException.getMessage());
        } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
            throw new GetCurrentPriceException("Call Interrupt: " + interruptedException.getMessage());
        } catch (NumberFormatException numberFormatException) {
            numberFormatException.printStackTrace();
            throw new GetCurrentPriceException("Conversion of Value not possible: " + numberFormatException.getMessage());
        }
    }
}
