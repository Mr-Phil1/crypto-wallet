# CryptoWallet
Dieses CryptoWallet-Projekt ist im Zuge der Programmierausbildung an dem IT-Kolleg Imst enstanden.

## Requirements
* Maven
* Java 16

## Used Ressources
* OpenJDK 16
* Gluon: [Gluon Start](start.gluon.io/)
  * für JavaFX 16
* CoinGecko API: [API-Documentation](https://www.coingecko.com/en/api)
* IntelliJ Community Edition
* Tutorial-Videos von der Lehrperson





## Bilder

|                                                            |
| ---------------------------------------------------------- |
| ![Error_cannot-load-BA](./img/Error_cannot-load-BA.png)    |
| Fehlermeldung bei einem unmöglichen Laden des BankAccounts |

|                                                          |
| -------------------------------------------------------- |
| ![Error_cannot-load-WL](./img/Error_cannot-load-WL.png)  |
| Fehlermeldung bei einem unmöglichen Laden der WalletList |

|                                                                                             |
| ------------------------------------------------------------------------------------------- |
| ![Error_insufficent-account-balance](./img/Error_insufficent-account-balance.png)           |
| Fehlermeldung bei einem zu geringen Kontostand, für den gewollten Kauf von Crypto-Einheiten |

|                                                                                                         |
| ------------------------------------------------------------------------------------------------------- |
| ![Error_insufficent-amount-of-crypto-in-wallet](./img/Error_insufficent-amount-of-crypto-in-wallet.png) |
| Fehlermeldung bei einem zu geringen Crypto-Stand für den gewollten Verkauf von Crypto-Einheiten         |

|                                                                                                     |
| --------------------------------------------------------------------------------------------------- |
| ![Exeption_cannot-load-BA](./img/Exeption_cannot-load-BA.png)                                       |
| Zusätzliche Ausgabe einer Diagnose-Fehlermeldung, wenn das Laden des BankAccounts nicht möglich ist |  |

|                                                                                                   |
| ------------------------------------------------------------------------------------------------- |
| ![Exeption_cannot-load-WL](./img/Exeption_cannot-load-WL.png)                                     |
| Zusätzliche Ausgabe einer Diagnose-Fehlermeldung, wenn das Laden der WalletList nicht möglich ist |

|                                                                                                               |
| ------------------------------------------------------------------------------------------------------------- |
| ![Exeption_insufficent-amount-of-crypto-in-wallet](./img/Exeption_insufficent-amount-of-crypto-in-wallet.png) |
| Zusätzliche Ausgabe einer Diagnose-Fehlermeldung, wenn man zu wenig Crypto-Einheiten für den Verkauf besitzt  |

|                                                                                                                          |
| ------------------------------------------------------------------------------------------------------------------------ |
| ![Exeptions_insufficent-account-balance](./img/Exeptions_insufficent-account-balance.png)                                |
| Zusätzliche Ausgabe einer Diagnose-Fehlermeldung, wenn man zu wenig Guthaben für einen Kauf von Crypto-Einheiten besitzt |

|                                                                     |
| ------------------------------------------------------------------- |
| ![Information-Dialog_Save-BA](./img/Information-Dialog_Save-BA.png) |
| Informationsausgaben wenn der BankAccount gespeichert wurde         |

|                                                                             |
| --------------------------------------------------------------------------- |
| ![Information-Dialog_Save-Wallet](./img/Information-Dialog_Save-Wallet.png) |
| Informationsausgaben wenn die WalletList gespeichert wurde                  |

|                                                               |
| ------------------------------------------------------------- |
| ![Input-Dialog_Buy-Wallet](./img/Input-Dialog_Buy-Wallet.png) |
| Eingabemaske für den Kauf von Crypto-Währungen                |

|                                                                 |
| --------------------------------------------------------------- |
| ![Input-Dialog_Sell-Wallet](./img/Input-Dialog_Sell-Wallet.png) |
| Eingabemaske für den Verkauf von Crypto-Währungen               |

|                                                               |
| ------------------------------------------------------------- |
| ![Input-Dialog_Deposit-BA](./img/Input-Dialog_Deposit-BA.png) |
| Eingabemaske für das Aufbuchen auf dem BankAccount            |

|                                                                 |
| --------------------------------------------------------------- |
| ![Input-Dialog_Withdraw-BA](./img/Input-Dialog_Withdraw-BA.png) |
| Eingabemaske für das Abbuchen von dem BankAccount               |

|                                                                     |
| ------------------------------------------------------------------- |
| ![Main-Wallet-Übersicht_leer](./img/Main-Wallet-Übersicht_leer.png) |
| Hauptübersicht ohne die verschiedenen Wallets                       |

|                                                                   |
| ----------------------------------------------------------------- |
| ![Main-Wallet-Übersicht_mit](./img/Main-Wallet-Übersicht_mit.png) |
| Hauptübersicht mit einem schon vorhandenen Crypto-Wallet          |

|                                         |
| --------------------------------------- |
| ![Wallet-Transactions_leer](./img/Wallet-Transactions_leer.png) |
| Wallet-Übersicht ohne Transactionen     |

|                                                               |
| ------------------------------------------------------------- |
| ![Wallet-Transactions_mit](./img/Wallet-Transactions_mit.png) |
| Wallet-Übersicht mit einer Kauf-Transaction                   |

|                                                                       |
| --------------------------------------------------------------------- |
| ![Wallet-Transactions_verkauf](./img/Wallet-Transactions_verkauf.png) |
| Wallet-Übersicht mit einer Kauf und Verkauf-Transaction               |
